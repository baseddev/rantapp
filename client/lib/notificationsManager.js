/**
 * Created by Jeff on 18/07/2015.
 */
// Notifications manager
MsgNotifications =
{
    value: [],
    updatePush: function(newVal)
    {
        this.value.push(newVal);
        Session.set('notifications', this.value.slice());
        flashTitle('New ' + (newVal.type == 'group' ? 'message' : newVal.type), 6);
    },
    clear: function(type, target)
    {
        this.value = this.value.filter(function(val)
        {
            return !(val.type == type && val.target == target);
        });
        Session.set('notifications', this.value.slice());
    },
    clearAll: function()
    {
        this.value = [];
        Session.set('notifications', this.value);
    },
    match: function(type, target)
    {
        return Session.get('notifications').some(function(val)
        {
            return val.type == type && (val.target == target || !target);
        });
    }
}