/**
 * Created by jeffmo on 8/07/15.
 */
// Set up default session variables
Session.setDefault('currentGroupId', -1);
Session.setDefault('notifications', []);
Session.setDefault('sessionId', -1);


// Helpers shared by more than one template
CommonHelpers = {
    getCurrentGroupName: function()
    {
        var currentId = Session.get('currentGroupId');
        if(currentId != -1)
        {
            var group = Groups.findOne({_id: currentId});

            if(!group)
                return {name: "group not found"}

            return {name: group._id, topic: group.topic};
        }
        else
            return {name: "no group"}

        // This just returns a random group id TODO: fix this up to use the actual group id of current group
        return currentId == -1 ? "No group" : currentId;
    },
};

SharedEvents = {
    chatInstance: {
        'click .reply-link': function(e)
        {

            $('#chat-input-prefix').attr('data-reply', "@" + this.from + "-" + this._id + " ").text('Cancel Reply');
            $('#chat-input-text').focus().attr('placeholder', "Replying " + this.from);
        },
        'click .reply-go': function(e)
        {
            var data = e.target.getAttribute('data');
            location.hash = '#' + data;

            var flashCount = 0;
            var intv;

            intv = setInterval(function()
            {
                if(flashCount > 4)
                    clearInterval(intv);

                if(flashCount % 2 == 0)
                    $('#-' + data).addClass("list-group-item-warning");
                else
                    $('#-' + data).removeClass("list-group-item-warning");

                flashCount++;

            }, 300)
        },
        'click .mute-link': function(e)
        {
            if(confirm("Are you sure you want to mute") == false)
                return;

            Meteor.call('toggleMute', this.from, true, Utils.getSessionId(), function(e,res)
            {
                MsgNotifications.clear('whisper', this.from);
                if(e)
                    alert(e);
            });
        }
    }
};


(function () {

    var original = document.title;
    var timeout;

    window.flashTitle = function (newMsg, howManyTimes) {
        function step() {
            document.title = (document.title == original) ? newMsg : original;

            if (--howManyTimes > 0) {
                timeout = setTimeout(step, 1000);
            }
        }

        howManyTimes = parseInt(howManyTimes);

        if (isNaN(howManyTimes)) {
            howManyTimes = 5;
        };

        cancelFlashTitle(timeout);
        step();
    };

    window.cancelFlashTitle = function () {
        clearTimeout(timeout);
        document.title = original;
    };

}());