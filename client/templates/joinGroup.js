/**
 * Created by jeffmo on 8/07/15.
 */
Template.joinGroup.events({

    // Joins group based on given preferences
    'submit #join-group-form': function(e)
    {
        var params = {topic: e.target.text.value.toUpperCase() == 'Random Topic'.toUpperCase() ? '' : e.target.text.value};
        if(e.target.groupSize.value)
            params.maxSize = parseInt(e.target.groupSize.value);
        else
        {
            alert("Please select a group size");
            return false;
        }

        var button = $('#join-group-button').button('loading')
        // business logic...

        if(e.target.isListener.checked)
        {
            Meteor.call('joinGroupListen', params, Utils.getSessionId(), function(err, res)
            {
                if(err)
                    alert(err);

                button.button('reset')
                Session.set("currentGroupId", res);
                Router.go('chat');
            });
        }
        else {
            Meteor.call('joinGroup', params, Utils.getSessionId(), function(err, res)
            {
                if(err)
                    alert(err);

                button.button('reset')
                Session.set("currentGroupId", res);
                Router.go('chat');
            });
        }

        e.target.text.value = "";
        e.target.groupSize.value = "";

        return false;
    },
    'click .topic-link': function(e)
    {
        $('#topic-input').val($(e.target).attr('data-topic'));

    }
});

Template.joinGroup.helpers(
    {
        groupSizes: Constants.GroupSizes
    }
)
