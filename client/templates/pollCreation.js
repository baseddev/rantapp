/**
 * Created by Jeff on 16/07/2015.
 */
Template.pollCreation.events(
    {
        'submit #create-poll-form': function (e)
        {
            var params = {maxChoices: 1, choices: e.target.choices.value.split(','), title: e.target.title.value};
            console.log(params);
            Meteor.call('createPoll', params, Session.get('currentGroupId'), Utils.getSessionId(), function(err, res)
            {
                if(!err)
                {
                    //Session.set('currentGroupId', this.groupId);
                    Router.go('chat');
                }
                else
                {
                    alert(err)
                }
            });



            return false;
        }
    }
)