/**
 * Created by Jeff on 11/07/2015.
 */
Template.manageWhispers.helpers({
    whispers: function()
    {
        var user = UserData.findOne();

        // Gets the whispers, and transform them so that we know if its from a muted user
        return Whispers.find().fetch().map(function(val)
        {
            var toUserId = val.user1 == user._id ? val.user2 : val.user1;
            console.log(toUserId);
            return {_id: val._id, toUserId: toUserId, isMuted: user.mutes.indexOf(toUserId) != -1, isHighlighted: MsgNotifications.match('whisper', toUserId)};
        });
    }
});

Template.manageWhispers.events(
    {
        'click .whisper-link': function (e)
        {
            MsgNotifications.clear('whisper',$(e.target).attr('data-id'));
        },
        'click .mute-link': function(e)
        {
            var that = this;

            if(confirm("Are you sure you want to mute") == false)
                return;

            Meteor.call('toggleMute', this.toUserId, true, Utils.getSessionId(), function(e,res)
            {
                MsgNotifications.clear('whisper', that.toUserId);
                if(e)
                    alert(e);
            });
        }
    }
)
