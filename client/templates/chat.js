/**
 * Created by jeffmo on 8/07/15.
 */
Template.chat.helpers({
    // Returns transformed chat contents of group
    groupChatContents: function()
    {
        MsgNotifications.clear('group', Session.get('currentGroupId'));
        MsgNotifications.clear('poll', Session.get('currentGroupId'));

        setTimeout(function()
        {
            $(".chatbox").scrollTop($(".chatbox")[0].scrollHeight);

        }, 100);

        var currentId = Session.get('currentGroupId');
        if (currentId == -1 || !Groups.findOne({_id: currentId}))
        {
            Router.go('joinGroup');
            return [{from: -1, text: 'notInGroup', isAdmin: true, itIs: function(){return false}, itIsNot: function(){return true}}];
        }
        else {
            var user = UserData.findOne();
            var group = Groups.findOne({_id: currentId}, {transform: function(g)
            {
                var namesSet = [];

                g.chatContents = g.chatContents.length > 0 ? g.chatContents.filter(function(value)
                {
                    return (user.mutes.indexOf(value.from) == -1)
                }).map(function (value)
                {
                    var truthy = [];


                    if(value.from == Constants.IDType.Poll)
                    {
                        truthy.push('poll');
                        //value.isPoll = true;
                        value.pollId = value.text;
                        value.title = value.data.title;
                    }
                    else if(value.from == Constants.IDType.AdminPoll)
                    {
                        truthy.push('admin', 'poll');
                        //value.isPoll = true;
                        value.pollId = value.text;
                        value.title = value.data.title;
                        //value.isAdmin = true;
                    }
                    else if(value.from == Constants.IDType.Admin)
                    {
                        truthy.push('admin');
                        value.isAdmin = true;
                    }
                    else if(value.from == user._id)
                    {
                        truthy.push('me');
                        value.isMe = true;
                    }
                    else // From ordinary user, we transform to get a friendly name
                    {
                        namesSet.pushSet(value.from);
                        //console.log(group.users, value.from)
                        value.friendlyName = Utils.getFriendlyName(namesSet.indexOf(value.from));
                    }

                    value.itIs = function(a,b,c,d,e,f,g)
                    {
                        for(var i = 0; i < arguments.length - 1; i++)
                        {
                            if(truthy.indexOf(arguments[i]) == -1)
                            {
                                return false;
                            }
                        }
                        return true;
                    };
                    value.itIsNot = function()
                    {
                        for(var i = 0; i < arguments.length - 1; i++)
                        {
                            if(truthy.indexOf(arguments[i]) != -1)
                            {
                                return false;
                            }
                        }
                        return true;
                    }

                    var transformedText = value.text.replace(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/,
                        "<a href='$&'>$&</a>").replace(/Kappa/gm, "<img src='https://static-cdn.jtvnw.net/emoticons/v1/25/1.0'>").replace(/@([a-zA-Z0-9]{16,17})-([a-zA-Z0-9]{16})/,
                        "<a class='reply-go' data='$2' href='javascript:;'>@$1</a>").replace(/FeelsBadMan/gm, "<img src='https://cdn.betterttv.net/emote/55678c247239dcf87b80d78b/2x'>");
                    value.text = Spacebars.SafeString(transformedText);

                    return value;
                }).slice(-Constants.MaxChatsClient)
                :
                [{from: -1, text: 'No messages in this group yet! Why not break the ice yourself~', isAdmin: true, itIs: function(){return false}, itIsNot: function(){return false}}];
                return g
            }});



            return group.chatContents;

        }
    },
    currentGroup: CommonHelpers.getCurrentGroupName,
    isListener: isListener
})

function isListener()
{
    var currentId = Session.get('currentGroupId');
    if (currentId == -1 || !Groups.findOne({_id: currentId})) {
        return false;
    }
    else
    {
        // TODO: Need to handle undefined stuff
        var thisGroup = Groups.findOne({_id: currentId});
        return !(thisGroup && thisGroup.maxSize > 0);
    }
}

function submitChat (e)
{
    var prefixElem = $('#chat-input-prefix');

    var chatText = $('#chat-input-text');

    if(!chatText.val())
        return false;

    Meteor.call('updateChat', Session.get('currentGroupId'), prefixElem.attr('data-reply') + chatText.val(), new Date(), Utils.getSessionId(), function(e, res)
    {
        if(e)
            alert(e);
    });

    chatText.val("");
    $(chatText).attr('placeholder', Constants.DefaultChat);
    prefixElem.attr('data-reply', "").text("");

    return false;
}

Template.chat.events({
    'click #delete-group-button': function(e)
    {
        Meteor.call('leaveGroup', Session.get('currentGroupId'), isListener(), Utils.getSessionId());
        Session.set('currentGroupId', -1);
        Router.go('joinGroup');
    },
    'submit #chat-input': submitChat,
    'click #chat-input-prefix': function(e)
    {
        $('#chat-input-prefix').attr('data-reply', '').text("");
        $('#chat-input-text').attr('placeholder', Constants.DefaultChat);
    },
    'click .whisper-link': function(e)
    {
        console.log($(e.target).attr('data-id'));

        Router.go('whisper', {toUserId: $(e.target).attr('data-id')});
        return false;
    },
    'click #chat-input-submit': submitChat
});

Template.chatInstance.events(
    SharedEvents.chatInstance
);


