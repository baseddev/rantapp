/**
 * Created by jeffmo on 10/07/15.
 */
Template.groupMembers.helpers({
    members: function()
    {
        var groupId = Session.get("currentGroupId");
        var myUser = UserData.findOne();

        // Returns a transformed members list of group

        if(groupId != -1)
        {
            var groupUsers = Groups.findOne({_id: groupId}).users;
            if(groupUsers)
                return groupUsers.map(function(val)
                {
                    return {toUserId: val, notSelf: val != myUser._id, isMuted: myUser.mutes.indexOf(val) != -1};
                });
            else
                return [{toUserId: "Unavailable for listening"}];
        }
        else
            return [];
    }
});

Template.groupMembersInstance.events({
    // Toggles muting of a user
    "change #mute-checkbox": function(e)
    {
        Meteor.call('toggleMute', this.toUserId, e.target.checked, Utils.getSessionId(), function(e,res)
        {
            if(e)
                alert(e);
        });
    }
})