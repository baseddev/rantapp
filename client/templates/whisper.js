/**
 * Created by Jeff on 11/07/2015.
 */
Template.whisper.helpers({
    whisperContents: function()
    {
        MsgNotifications.clear('whisper', this.user1);
        MsgNotifications.clear('whisper', this.user2);

        setTimeout(function()
        {
            $(".chatbox").scrollTop($(".chatbox")[0].scrollHeight);

        }, 100);

        if (this.chatContents)
        {
            var user = UserData.findOne();

            return this.chatContents.slice(-Constants.MaxChatsClient).map(function (value)
            {

                if(user._id == value.from)
                    value.isMe = true;

                var transformedText = value.text.replace(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/,
                    "<a href='$&'>$&</a>").replace(/Kappa/gm, "<img src='https://static-cdn.jtvnw.net/emoticons/v1/25/1.0'>").replace(/@([a-zA-Z0-9]{16,17})-([a-zA-Z0-9]{16})/,
                    "<a class='reply-go' data='$2' href='javascript:;'>@$1</a>").replace(/FeelsBadMan/gm, "<img src='https://cdn.betterttv.net/emote/55678c247239dcf87b80d78b/2x'>");
                value.text = Spacebars.SafeString(transformedText);

                return value;
            });
        }
        else
        {
            return [{from: '', text: "LOADING"}];
        }
    },
    toUser: function()
    {
        var myId = UserData.findOne()._id;
        return myId == this.user1 ? this.user2 : this.user1;
    }
});

function submitChat (e)
{
    var prefixElem = $('#chat-input-prefix');

    var chatText = $('#chat-input-text');

    if(!chatText.val())
        return false;

    var toUser = this.user1 == UserData.findOne()._id ? this.user2 : this.user1;

    console.log(toUser);
    Meteor.call('updateWhisper', toUser, prefixElem.attr('data-reply') + chatText.val(), Utils.getSessionId(), function(e,res)
    {
        if(e)
            alert(e);
    });

    chatText.val("");
    $(chatText).attr('placeholder', Constants.DefaultChat);
    prefixElem.attr('data-reply', "").text("");

    return false;
}

Template.whisper.events({
    'submit #chat-input': submitChat,
    'mouseenter #chat-area': function(e)
    {
        // Clears any whisper notifications related to the current whispering user
        MsgNotifications.clear('whisper', this.user1);
        MsgNotifications.clear('whisper', this.user2);
    },
    'click #chat-input-prefix': function(e)
    {
        $('#chat-input-prefix').attr('data-reply', '').text("");
        $('#chat-input-text').attr('placeholder', Constants.DefaultChat);
    },
    'click #chat-input-submit': submitChat
});

Template.whisperInstance.events(SharedEvents.chatInstance);