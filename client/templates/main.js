/**
 * Created by jeffmo on 8/07/15.
 */

var handles;


/*
 * Creating a session identifier, if none
 * Gets the identifier which is generated server side
 * Also subscribes to groups according to the sessionId
 */
//if(!Meteor.loggingIn() && !Meteor.user() && !Utils.getSessionId())
//{
//    console.log("Initialising session id");
//    setUpTempUser();
//}
//else
//{
//    handles = setUpSubscriptions(Utils.getSessionId());
//}
//

function stopHandles()
{
    handles.forEach(function(v)
    {
        v.stop();
    })
}


// Autoruns this subscription whenever the user id changes
Tracker.autorun(function()
{


    // Clear notificaitons
    MsgNotifications.clearAll();

    // Stop the observeChanges handles
    if(handles)
    {
        stopHandles();
    }

    console.log("User Changed. Session ID: " + Session.get('sessionId'));

    // Create new temp user if none
    if(!Meteor.user() && !Utils.getSessionId())
    {
        setUpTempUser();
        return;
    }
    else
    {
        Session.set('sessionId', Utils.getSessionId());
        // Set up the subscriptions and observations again
        handles = setUpSubscriptions(Utils.getSessionId());
    }


});

// Sets up the temporary user
function setUpTempUser()
{
    Meteor.call('generateIdentifier', function(err, res)
    {
        if(err)
            alert("Cannot initialise chat. Try reloading?")

        localStorage.setItem("sessionId", res);
        Session.set('sessionId', res);

        console.log("session id set to " + res);
    })
}



/*
 * Set up subscriptions
 * And also auto-runs subscribing when group status changes
 */
function setUpSubscriptions(sessionId)
{
    console.log("setting up subscriptions for: " + sessionId)

    Meteor.subscribe("userData", sessionId);
    Meteor.subscribe('whispers', sessionId);
    Meteor.subscribe('groups', sessionId);
    Meteor.subscribe('polls', sessionId);
    Meteor.subscribe('groupsListen', sessionId);

    // Runs functions whenever the client's observable Whispers changes (i.e. chat added)
    var whisperHandle = Whispers.find().observeChanges(
    {
        changed: function(id, changedFields)
        {
            var user = UserData.findOne();

            // If message not self initiated
            var from = changedFields.chatContents[changedFields.chatContents.length - 1].from;
            if(from != user._id)
            {
                if(user.mutes.indexOf(from) == -1)
                    MsgNotifications.updatePush(new Classes.Notification('whisper', changedFields.chatContents[changedFields.chatContents.length - 1].from));
            }
        }
    });

    // Runs functions whevever the client's observable Groups' fields changes (i.e. chat added)
    var groupsHandle = Groups.find().observeChanges(
    {
        changed: function(id, changedFields)
        {
            // Only push notification if chat changes and is not self initiated
            if(changedFields.chatContents)
            {
                var last = changedFields.chatContents[changedFields.chatContents.length - 1];
                var from = last.from;
                var user = UserData.findOne();

                console.log(last)

                if (from == Constants.IDType.Poll)
                {
                    if (user.mutes.indexOf(last.data.initiator) == -1)
                        MsgNotifications.updatePush(new Classes.Notification('poll', id));
                }
                else if (from != UserData.findOne()._id)
                {
                    if(user.mutes.indexOf(from) == -1)
                        MsgNotifications.updatePush(new Classes.Notification('group', id));
                }

                console.log(user.mutes.indexOf(from))

            }

            // Later on we can have notification on user join or poll or whatever
        }
    });

    return [whisperHandle, groupsHandle];
}

Template.main.helpers({
    groups: function()
    {
        var bob = Session.get('notifications');
        console.log("triggered");

        return Groups.find({}).fetch().map(function(val)
        {
            if(MsgNotifications.match('group', val._id) || MsgNotifications.match('poll', val._id))
                val.isHighlighted = true;

            return val;
        });
    },
    isReady: function()
    {
        return Session.get('sessionId') != -1;
    },
    currentGroup: CommonHelpers.getCurrentGroupName,
    isInGroup: function()
    {
        return Session.get('currentGroupId') !== -1;
    },
    newWhispers: function()
    {
        return MsgNotifications.match('whisper');
    }
});

Template.main.events(
    {
        'click #menu-toggle': function(e)
        {
            e.preventDefault();
            $("#wrapper").toggleClass("active");
        }
    }
);

//,
//'click #menu-toggle': function(e)
//{
//    e.preventDefault();
//    alert("SDF");
//    $("#wrapper").toggleClass("active");
//}

function logRenders () {
    for(var template in Template)
    {
        if(!template.renderFunction)
            return;
        var oldRender = template.renderFunction;
        var counter = 0;

        template.onRendered(function () {
            console.log(name, "render count: ", ++counter);
            oldRender && oldRender.apply(this, arguments);
        });
    };
};

logRenders();