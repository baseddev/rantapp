/**
 * Created by jeffmo on 9/07/15.
 */
Template.home.events(
    {
        "click #clear-session": function()
        {
            localStorage.clear();
            Session.set('sessionId', -1)
        }
    }
)
