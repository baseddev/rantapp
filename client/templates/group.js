/**
 * Created by jeffmo on 7/07/15.
 */
Template.group.events({
    'click .group-button': function(e)
    {
        MsgNotifications.clear('group', this._id);
        MsgNotifications.clear('poll', this._id);

        Session.set('currentGroupId', this._id)
        Router.go('chat');
    }
})