/**
 * Created by Jeff on 16/07/2015.
 */
Template.pollView.events(
    {
        'submit #voting-form': function(e)
        {
            var index = this.choices.indexOf(e.target.choice.value);
            if(index == -1)
            {
                alert("Please select at least 1");
                return false;
            }

            Meteor.call('votePoll', [index], this._id, Utils.getSessionId(), function(e, res)
            {
                if(e)
                    alert(e);
            });
            return false;
        },
        'click #forfeit-button': function(e)
        {
            Meteor.call('votePoll', [], this._id, Utils.getSessionId(), function(e,res)
            {
                if(e)
                    alert(e);
            });

            return false;
        },
        'click #back-button': function(e)
        {
            Router.go('chat');
        }
    }
)