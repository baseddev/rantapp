/**
 * Created by jeffmo on 7/07/15.
 */

// Routes the addresses to the right templates, and with the correct data

Router.configure(
    {
        layoutTemplate: 'main',
        loadingTemplate: 'loading',
        notFoundTemplate: 'notFound'
    }
)

Router.route('/', {
    name: 'joinHome',
    template: 'joinGroup'
});

Router.route('/clear', {
    name: 'home',
    template: 'home'
});

Router.route('/join', {
    name: "joinGroup",
    template: "joinGroup",
    onBeforeAction: function(){
        Session.set('popularTopics', {isDone: false, topics: undefined});

        Meteor.call('aggregateTopics', 10, Utils.getSessionId(), function(e,res)
        {
            if(e)
                alert(e);

            Session.set('popularTopics', {isDone: true, topics: res, defaultTopics: Constants.DefaultTopics})
        });
        this.next();
    },
    data: function()
    {
        var out = Session.get('popularTopics');
        console.log(out);
        return out;
    }
});

Router.route('/join/:id', function()
{
    Meteor.call('joinGroupListen', {_id: this.params.id}, Utils.getSessionId(), function(err, res)
    {
        if(err)
            Log.error(err);
        else
        {
            Session.set('currentGroupId', res);
            Router.go('/chat');
        }
    });

});

Router.route('/chat', {
    name: "chat",
    template: "chat"
})

Router.route('/members', {
    name: "members",
    template: "groupMembers"
})

Router.route('/create-poll', {
    name: 'pollCreation',
    template: 'pollCreation'
});

Router.route('/create-admin-poll',
{
    name:'pollCreationAdmin',
    template: 'pollCreationAdmin'
});

Router.route('test',{name: 'test'});

Router.route('/view-poll/:pollId', {
    name: 'pollView',
    template: 'pollView',
    waitOn: function()
    {
        return Meteor.subscribe('polls', Utils.getSessionId());
    },
    data: function()
    {
        var poll = Polls.findOne({_id: this.params.pollId});
        if(!poll)
        {
            alert("This poll is not ready yet!");
            Router.go('/chat');
            return;
        }

        var sums = [];
        poll.choices.forEach(function()
        {
            sums.push(0);
        });

        var userId = UserData.findOne()._id;
        var hasVoted = poll.votes != undefined;


        if(hasVoted)
        {
            console.log(poll.votes.length);

            var totalVotes = 0;
            poll.votes.forEach(function(val)
            {
                if(val.length > 0)
                {
                    totalVotes++;

                    val.forEach(function(i)
                    {
                        sums[i] = sums[i] + 1;
                    });
                }
            });
            return {sums: sums.map(function(val,i)
            {
                return [poll.choices[i], val];
            }), totalVotes: totalVotes, hasVoted: true, title: poll.title, _id: this.params.pollId};
        }
        else
            return poll;
    }
});

Router.route('/browse-topics', {
    name: 'popularTopics',
    onBeforeAction: function(){
        Session.set('popularTopics', {isDone: false, topics: undefined});

        Meteor.call('aggregateTopics', 10, Utils.getSessionId(), function(e,res)
        {
            if(e)
                alert(e);

            Session.set('popularTopics', {isDone: true, topics: res, defaultTopics: Constants.DefaultTopics})
        });
        this.next();
    },
    data: function()
    {
        var out = Session.get('popularTopics');
        console.log(out);
        return out;
    }
});

Router.route('/login', {
    name: 'login'
})


Router.route('/whisper/:toUserId', {
    name: 'whisper',
    template: 'whisper',
    waitOn: function()
    {
        return Meteor.subscribe('whispers', Utils.getSessionId());
    },
    data: function()
    {
        Meteor.call('initiateWhisper', this.params.toUserId, Utils.getSessionId());

        // TODO: Currently if 2 users are both in 2 chats together and they whisper, their whispers will be combined into one

        return Whispers.findOne({$or: [{user1: this.params.toUserId}, {user2: this.params.toUserId}]});
    }
})

Router.route('/manage-whispers', {
    name: 'manageWhispers',
    template: 'manageWhispers'
})