/**
 * Created by Jeff on 18/07/2015.
 */
// Some constants for both client and server
Constants =
{
    GroupSizes: [12, 5, 50],
    DefaultGroupSize: 12,
    IDType: {AdminPoll: -3, Poll: -2, Admin: -1},
    DefaultTopics: ["ANYTALK", "PHILOSOPHY", "LIFE", "RELATIONSHIPS"],
    DefaultChat: "Enter chat stuff here",
    MaxChats: 50,
    MaxChatsClient: 25
}