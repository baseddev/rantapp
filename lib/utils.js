/**
 * Created by jeffmo on 8/07/15.
 */
// Logging object for client and server
// Probably shouldnt use it cus its bad
Log = {
    error: function()
    {
        console.log(arguments);

        if(Meteor.isClient)
            alert("An error has occured: " + arguments);
    },
    info: function()
    {
        console.log(arguments);
    }
}


// Useful utilities
Utils =
{
    getSessionId: function()
    {
        var seshId = localStorage.getItem("sessionId");

        return seshId;
    },
    getFriendlyName: function (index)
    {
        var colours = ["Blue", "Red", "Yellow", "Green", "Purple", "Orange", "Turquoise"];
        var animals = ["Tiger", "Lion", "Dog", "Cat", "Mouse", "Sheep", "Cow", "Elephant", "Tortoise"];

        if(index >= colours.length * animals.length)
            throw new Error("Not enough combos to handle this large index");

        return (colours[index % colours.length] + " " + animals[index / animals.length | 0]).toUpperCase();
    }
};

Array.prototype.pushSet = function(val)
{
    return (this.indexOf(val) == -1) ? this.push(val) : -1;
}

