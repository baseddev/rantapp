/**
 * Created by Jeff on 18/07/2015.
 */

// Basically object schemas

Classes = {
    Chat: function(from, text, timestamp, data, _id)
    {
        this.from = from;
        this.text = text;
        this.timestamp = timestamp;

        if(_id)
            this._id = _id;
        else
            this._id = Random.id(16);

        if(data)
            this.data = data;
    },
    // Note that for notifications, the target param is the toUser for type whisper and the groupId for type group
    Notification: function(type, target)
    {
        this.type = type;
        this.target = target;
    }
}
