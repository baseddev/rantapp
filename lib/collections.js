/**
 * Created by jeffmo on 7/07/15.
 */

// Declares the collections (databases) to be used
Groups = new Mongo.Collection("groups");
Identifiers = new Mongo.Collection("identifiers");
UserData = new Mongo.Collection("userData");
Whispers = new Mongo.Collection("whispers");
Polls = new Mongo.Collection("polls");