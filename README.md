# README #

This application is still in development.

If you would like to check it out, install Meteor (version 1.1 for compatibility) from http://meteor.com
then 
```
#!bash

git clone https://mdl941027@bitbucket.org/baseddev/rantapp.git
cd rantapp
meteor
```

and navigate to the address specified in the terminal