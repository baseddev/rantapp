// On meteor start up

// Profiling
//Kadira.connect('SR8yTfjYR4vmprixR', 'ced1928f-75c1-42f2-8c4a-70b921f6a6c6');

/*
 * When creating user with the meteor accounts system, link it to our custom UserData collection via id
 */
Accounts.onCreateUser(function(options, user)
{
    var id = Random.id(16);

    // Create our custom UserData entry
    UserData.insert({_id: id, groups: [], mutes: []}, function(err, userId)
    {
        if(err)
            Log.error(err);
    });

    user.userId = id;

    return user;
});

