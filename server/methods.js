// Put client and server methods here
Meteor.methods(
    {
        aggregateTopics: function(limit, identifier)
        {
            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;

            if(limit > 1000)
                limit = 1000;

            var results = Groups.aggregate([
                {$group: {_id: '$topic', total: {$sum: 1}}},
                {$limit: limit},
                {$sort: {total: -1}}
            ]);

            return results;
        },
        createAdminPoll: function(parameters)
        {
            console.log("Creating admin poll")


            if(!Meteor.user().isAdmin)
                throw new Meteor.Error("Only admins authorised");
            if(!parameters.title)
                throw new Meteor.Error("No title given");
            if(!parameters.choices || parameters.choices.length < 2)
                throw new Meteor.Error("Need at least 2 choiecs in poll");
            if(!(parameters.maxChoices >= 1 && parameters.maxChoices < parameters.choices.length))
                throw new Meteor.Error("Invalid number of choices")

            var userId = Meteor.userId();

            var allGroups = Groups.find().fetch();
            var date = new Date();
            var pollId = Polls.insert({title: parameters.title, votes: [], voted: [], choices: parameters.choices, maxChoices: parameters.maxChoices, group: Constants.IDType.AdminPoll, initiator: userId});
            allGroups.forEach(function(group)
            {
                Groups.update({_id: group._id}, {
                    $push: {chatContents: new Classes.Chat(Constants.IDType.AdminPoll, pollId, date, {title: parameters.title, initiator: userId})}
                });
            });

        },
        // Creates a poll in the specified group
        createPoll: function(parameters, groupId, identifier)
        {
            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;

            var group = Groups.findOne({_id: groupId, users: userId});

            if(!group)
                throw new Meteor.Error("Group with user not found");


            console.log(parameters.choices.length);

            if(!parameters.title)
                throw new Meteor.Error("No title given");
            if(!parameters.choices || parameters.choices.length < 2)
                throw new Meteor.Error("Need at least 2 choiecs in poll");
            if(!(parameters.maxChoices >= 1 && parameters.maxChoices < parameters.choices.length))
                throw new Meteor.Error("Invalid number of choices")

            var pollId = Polls.insert({title: parameters.title, votes: [], voted: [], choices: parameters.choices, maxChoices: parameters.maxChoices, group: groupId, initiator: userId});

            // Notifies the group of the poll via message
            Groups.update({_id: groupId}, {
                $push: {chatContents: new Classes.Chat(Constants.IDType.Poll, pollId, new Date(), {title: parameters.title, initiator: userId})}
            });

            return pollId;
        },
        votePoll: function(choices, pollId, identifier)
        {
            // Allow relinquishing vote

            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;

            var poll = Polls.findOne({_id: pollId});
            if(!poll)
                throw new Meteor.Error("No poll with given ID found");

            //var i = poll.votes.length - 1;
            //for(; i >= 0; i--)
            //{
            //    if()
            //}
            //
            console.log(poll.group + " " + userId);
            var group = Groups.findOne({_id: poll.group, users: userId});
            if(!group && poll.group != Constants.IDType.AdminPoll)
                throw new Meteor.Error("You are not allowed to vote in this poll");

            if(poll.voted.indexOf(userId) != -1)
                throw new Meteor.Error("You have already voted");

            if(choices.length > poll.maxChoices)
                throw new Meteor.Error("Too many choices supplied")


            Polls.update({_id: pollId}, {
                $push: {votes: choices, voted: userId}
            });
            return true;

        },
        // Controls muting of the toUserId by the user
        toggleMute: function(toUserId, doMute, identifier)
        {
            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;

            if(!toUserId)
                throw new Meteor.Error("No to-user given");


            var toUser = UserData.findOne({_id: toUserId});
            if(!toUser)
                throw new Meteor.Error("No to-user found");

            if(doMute)
            {
                var user = UserData.findOne({_id: userId});
                if(user.mutes.indexOf(toUserId) != -1)
                    return;
                else
                    UserData.update({_id: userId}, {$push: {mutes: toUserId}});
            }
            else
            {
                UserData.update({_id: userId}, {$pull: {mutes: toUserId}});
            }


        },
        // TODO: Not throw error when whisper already exists
        // Initiates a whisper from the user to the toUserId
        initiateWhisper: function(toUserId, identifier)
        {
            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;

            if(!toUserId)
                throw new Meteor.Error("No to-user given");


            var toUser = UserData.findOne({_id: toUserId});
            if(!toUser)
                throw new Meteor.Error("No to-user found");


            if(userId == toUserId)
                throw new Meteor.Error("cannot whisper to self");

            // Finds existing whispers between the given 2 users
            var existingWhispers = Whispers.find({$and: [{$or: [{user1: userId}, {user2: userId}]}, {$or: [{user1: toUserId}, {user2: toUserId}]}]}).fetch();

            if(existingWhispers.length > 0)
                throw new Meteor.Error("Whisper with same users already exist");

            return Whispers.insert({user1: userId, user2: toUserId, chatContents: []});
        },
        // Updates the whisper of the specified ID with the given text
        updateWhisper: function(toUserId, text, identifier)
        {
            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;

            var whisper = Whispers.findOne({$and: [{$or: [{user1: userId}, {user2: userId}]}, {$or: [{user1: toUserId}, {user2: toUserId}]}]});
            if(!whisper)
            {
                throw new Meteor.Error("Whisper id not found");
                return;
            }

            console.log(whisper);
            console.log(userId);
            console.log(toUserId);

            // Check for user's membership in the whisper
            if(whisper.user1 == toUserId || whisper.user2 == toUserId)
            {
                Whispers.update({_id: whisper._id}, {
                    $push: {chatContents: new Classes.Chat(userId, text, new Date())}
                });
                if(whisper.chatContents.length > Constants.MaxChats)
                    Whispers.update({_id: whisper._id},{$pop: {chatContents: -1}});
            }
            else
                throw new Meteor.Error("User not in whisper");
        },
        // Leaves a group
        leaveGroup: function(groupId, isListener, identifier)
        {
            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;



            var group = Groups.findOne({_id: groupId});

            if(!group) {
                Log.error("Group not found");
                return;
            }

            console.log("leaving group");


            UserData.update({_id: userId}, {
                $pull: {groups: group._id}
            });


            // Removes the group if the person leaving was the last person
            if(group.users.length <= 1)
            {
                Groups.remove({_id: groupId});
            }
            else
            {
                if(isListener)
                    Groups.update({_id: groupId}, {
                        $pull: {listeners: userId},
                    });
                else
                    // Pulls the user from the group's user array
                    Groups.update({_id: groupId}, {
                        $pull: {users: userId},
                        $set: {joinable: (group.users.length - 1) < group.maxSize}
                    });
            }

        },
        // Updates the group chat
        updateChat: function(groupId, text, date, identifier)
        {
            // TODO: Chanmge date to be set server side
            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;

            var group = Groups.findOne({_id: groupId});

            // Check for membership in group
            // Best is to check both user in group AND group in user. Maybe do that later
            if(group && group.users.indexOf(userId) != -1)
            {
                Groups.update({_id: groupId}, {
                    $push: {chatContents: new Classes.Chat(userId, text, new Date())}
                });
                if(group.chatContents.length > Constants.MaxChats)
                    Groups.update({_id: groupId}, {$pop: {chatContents: -1}});
            }


            else
                Log.error("Group id not found");
        },
        // Generates a temporary identifier for a user without an account
        generateIdentifier: function()
        {
            var id = Random.id(16);

            // Link the identifier with the group
            UserData.insert({groups: [], mutes: []}, function(err, userId)
            {
                if(err)
                    Log.error(err);

                Identifiers.insert({identifier: id, userId: userId});

            });

            return id;
        },
        joinGroupListen: function(parameters, identifier)
        {
            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;

            var query = {listeners: {$nin: [userId]}, users: {$nin: [userId]}};
            if(parameters.topic)
                query.topic = parameters.topic.toUpperCase();
            if(parameters._id)
                query._id = parameters._id;

            // Find up to 100 joinable groups with corresponding size requirements
            var groups = Groups.find(query, {limit: 100}).fetch();
            var groupCount = groups.length;

            // Check if existing group matches the user's requirements
            if(groupCount == 0)
            {
                throw new Meteor.Error("No groups with given topic to listen in to! Note that you cannot join a group you are already in, either as a user and a listener")
            }
            else
                {
                    // Joins a random group
                    var groupToJoin = groups[Math.floor(Math.random() * groupCount)];

                    // Update the group's parameters
                    Groups.update({_id: groupToJoin._id}, {
                        $push: {listeners: userId},
                        $set: {joinable: groupToJoin.users.length < groupToJoin.maxSize}
                    });

                    // Updating the user's group affiliations
                    UserData.update({_id: userId}, {$push: {groups: groupToJoin._id}});

                    return groupToJoin._id;
                }
        },
        // Joins a group given the parameters
        joinGroup: function(parameters, identifier)
        {
            var userId;
            if(!Meteor.user())
            {
                if(!identifier)
                    throw new Meteor.Error("No identifier or user given")
                else
                {
                    var ident = Identifiers.findOne({identifier: identifier});
                    if(!ident) {
                        Log.error("User not found");
                        return;
                    }
                    userId = ident.userId;
                }
            }
            else
                userId = Meteor.user().userId;

            if(parameters.maxSize && (Constants.GroupSizes.indexOf(parameters.maxSize) == -1)) {
                Log.error("Invalid group size");
                return;
            }


            // TODO: Possible concurrency issue here, allowing potential people to join even though group is no longer joinable

            var query = {joinable: true, users: {$nin: [userId]}, listeners: {$nin: [userId]}};
            if(parameters.maxSize)
                query.maxSize = parameters.maxSize;
            if(parameters.topic)
            {
                if(parameters.topic.length <= 16)
                    query.topic = parameters.topic.toUpperCase();
                else
                    throw new Meteor.Error("Topic needs to be less than 16 characters")
            }

            console.log(query);

            // Find up to 100 joinable groups with corresponding size requirements
            var groups = Groups.find(query, {limit: 100}).fetch();
            var groupCount = groups.length;

            // Check if existing group matches the user's requirements
            if(groupCount == 0)
            {
                // Creating new group with requirements and with the userid added to it
                return Groups.insert({users: [userId], chatContents: [], maxSize: query.maxSize ? query.maxSize : Constants.DefaultGroupSize, topic: query.topic ? query.topic : "ANYTALK", joinable: true, listeners: []}, function(err, groupId)
                {
                    if(err)
                        Log.error(err);

                    // Update the user's group affiliations
                    UserData.update({_id: userId}, {$push: {groups: groupId}});
                })
            }
            else
            {
                // Joins a random group
                var groupToJoin = groups[Math.floor(Math.random() * groupCount)];

                // Update the group's parameters
                Groups.update({_id: groupToJoin._id}, {
                    $push: {users: userId},
                    $set: {joinable: groupToJoin.users.length < groupToJoin.maxSize}
                });

                // Updating the user's group affiliations
                UserData.update({_id: userId}, {$push: {groups: groupToJoin._id}});

                return groupToJoin._id;
            }
        }
    }
);
