// Published collections


/*
 * Publish only the groups owned by the user
 */
Meteor.publish('groups', function(identifier)
{
    var userId;
    if(!this.userId)
    {
        if(!identifier)
            throw new Meteor.Error("No identifier or user given")
        else
        {
            var ident = Identifiers.findOne({identifier: identifier});
            if(!ident)
                throw new Meteor.Error("User with identifier not found");
            userId = ident.userId;
        }
    }
    else
        userId = Meteor.users.findOne({_id: this.userId}).userId;


    // Handles when a temporary user (i.e. one that does not have an account) leaves
    this._session.socket.on("close", Meteor.bindEnvironment(function(err, res)
    {
        // We basically clean up and remove them from the groups they were in
        // and later remove their Identifiers and UserData

        var groups = Groups.find({users: userId}).fetch();

        groups.forEach(function(group)
        {
            if(group.users.length <= 1)
            {
                Groups.remove({_id: group._id});
            }
            else
            {
                // Put them in listeners? So they can leave if they want
                Groups.update({_id: group._id}, {
                    $pull: {users: userId},
                    $push: {listeners: userId},
                    $set: {joinable: (group.users.length - 1) < group.maxSize}
                });
            }
        });

        UserData.update({_id: userId}, {$set: {groups: []}});

        //Identifiers.remove({identifier: identifier});
        //UserData.remove({_id: userId});
    }, function(e){
        console.log(e)
    }));

    // Returns the things visible for the client
    return Groups.find({users: userId}, {fields: {chatContents: {$slice: -Constants.MaxChats}}});
});
Meteor.publish('groupsListen', function(identifier)
{
    var userId;
    if(!this.userId)
    {
        if(!identifier)
            throw new Meteor.Error("No identifier or user given")
        else
        {
            var ident = Identifiers.findOne({identifier: identifier});
            if(!ident)
                throw new Meteor.Error("User with identifier not found");
            userId = ident.userId;
        }
    }
    else
        userId = Meteor.users.findOne({_id: this.userId}).userId;

    // Returns the things visible for the client
    return Groups.find({listeners: userId}, {fields: {chatContents: 1, topic: 1, users: 1}});
});

Meteor.publish("polls", function (identifier) {
    var self = this;


    var userId;
    if(!this.userId)
    {
        if(!identifier)
            throw new Meteor.Error("No identifier or user given")
        else
        {
            var ident = Identifiers.findOne({identifier: identifier});
            if(!ident)
                throw new Meteor.Error("User with identifier not found");
            userId = ident.userId;
        }
    }
    else
        userId = Meteor.users.findOne({_id: this.userId}).userId;

    // When user's group affiliations change
    // Add polls from the new group
    var handle2 = UserData.find({_id: userId}).observeChanges(
        {
            // TODO: Remove polls on client side when group is left
            changed: function(id, fields)
            {
                console.log(fields)
                if(fields.groups)
                {
                    Polls.find({$or: [{group: {$in: fields.groups}}, {group: Constants.IDType.AdminPoll}]}).fetch().forEach(function(fields)
                    {
                        var group = Groups.findOne({_id: fields.group});
                        var isListener = !group || group.listeners.indexOf(userId) != -1;

                        console.log(fields)
                        if(fields.voted.indexOf(userId) != -1)
                            self.added('polls', fields._id, fields);
                        else if(isListener)
                            self.added('polls', fields._id, {title: fields.title, group: fields.group, initiator: fields.initiator, choices: fields.choices, maxChoices:fields.maxChoices, votes: fields.votes})
                        else
                            self.added('polls', fields._id, {title: fields.title, group: fields.group, initiator: fields.initiator, choices: fields.choices, maxChoices:fields.maxChoices})
                    })
                }
            }
        }
    )


    // Add polls when new poll created in the user's groups
    var handle = Polls.find({}).observeChanges({
        added: function (id, fields)
        {
            var userGroups = UserData.findOne({_id: userId}).groups; //Groups.find({users: userId}).fetch();
            if(userGroups.indexOf(fields.group) != -1 || fields.group == Constants.IDType.AdminPoll)
            {
                var group = Groups.findOne({_id: fields.group});
                var isListener = !group || group.listeners.indexOf(userId) != -1;
                if(isListener)
                    self.added('polls', id, {title: fields.title, group: fields.group, initiator: fields.initiator, choices: fields.choices, maxChoices:fields.maxChoices, votes:fields.votes});
                else
                    self.added('polls', id, {title: fields.title, group: fields.group, initiator: fields.initiator, choices: fields.choices, maxChoices:fields.maxChoices});
            }
        },
        changed: function (id, fields)
        {
            if(fields.voted.indexOf(userId) != -1)
                self.changed('polls', id, fields);
        },
        removed: function (id, fields)
        {
            self.removed('polls', id);
        }
    });

    self.ready();

    // Stop observing the cursor when client unsubs.
    // Stopping a subscription automatically takes
    // care of sending the client any removed messages.
    self.onStop(function () {
        handle2.stop();
        handle.stop();
    });
});

/*
 * Publish the whispers associated with the user
 */
Meteor.publish('whispers', function(identifier)
{
    var userId;
    if(!this.userId)
    {
        if(!identifier)
            throw new Meteor.Error("No identifier or user given")
        else
        {
            var ident = Identifiers.findOne({identifier: identifier});
            if(!ident)
                throw new Meteor.Error("User with identifier not found");
            userId = ident.userId;
        }
    }
    else
        userId = Meteor.users.findOne({_id: this.userId}).userId;

    return Whispers.find({$or: [{user1: userId}, {user2: userId}]});
});

/*
 * Publish the UserData associated with the user
 */
Meteor.publish('userData', function(identifier)
{
    var userId;
    if(!this.userId)
    {
        if(!identifier)
            throw new Meteor.Error("No identifier or user given")
        else
        {
            var ident = Identifiers.findOne({identifier: identifier});
            if(!ident)
                throw new Meteor.Error("User with identifier not found");
            userId = ident.userId;
        }
    }
    else
        userId = Meteor.users.findOne({_id: this.userId}).userId;


    return UserData.find({_id: userId});
})